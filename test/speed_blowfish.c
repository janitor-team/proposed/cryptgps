#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include "blowfish.h"



main () {
    BF_KEY k;
    unsigned char in[1024*1024];
    unsigned char out[1024*1024];
    unsigned char ivec[8];
    int i;
    struct timeval tv;
    double t1,t2;

    BF_set_key(&k, 16, "abcdefghijklmnop");


    /* CBC TEST */
    

    for (i=0; i<8; i++) ivec[i] = 0;
    for (i=0; i<1024*1024; i++) in[i] = ' ';

    gettimeofday(&tv, NULL);
    t1 = tv.tv_sec + tv.tv_usec * 1E-6;

    BF_cbc_encrypt(in, out, 1024*1024, &k, ivec, 1);

    gettimeofday(&tv, NULL);
    t2 = tv.tv_sec + tv.tv_usec * 1E-6;

    printf("%f\n", t2-t1);
}
