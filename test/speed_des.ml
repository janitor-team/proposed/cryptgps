open Crypt_des;;
open Cryptsystem;;
open Cryptmodes;;
let k = prepare (set_parity "abcdefgh");;
let s = String.make (1024*1024) ' ';;

let x0 = Sys.time() in
ignore(encrypt_cbc k (0,0,0,0) s);
print_float (Sys.time() -. x0);
print_newline()
;;
