(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

let quadruple_of_int32 xl xr =
  let xl_msb = Int32.to_int(Int32.shift_right_logical xl 16) in
  let xl_lsb = (Int32.to_int xl) land 0xffff in
  let xr_msb = Int32.to_int(Int32.shift_right_logical xr 16) in
  let xr_lsb = (Int32.to_int xr) land 0xffff in
  (xl_msb,xl_lsb,xr_msb,xr_lsb)
;;


let int32_of_quadruple (xl_msb,xl_lsb,xr_msb,xr_lsb) ret_xl ret_xr =
  let xl = 
    Int32.logor
      (Int32.of_int xl_lsb)
      (Int32.shift_left (Int32.of_int xl_msb) 16)
  in
  let xr = 
    Int32.logor
      (Int32.of_int xr_lsb)
      (Int32.shift_left (Int32.of_int xr_msb) 16)
  in
  ret_xl := xl;
  ret_xr := xr
;;

(* ======================================================================
 * History:
 * 
 * $Log$
 * 
 *)
