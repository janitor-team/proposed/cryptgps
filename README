-------------------------------------------------
cryptgps: some symmetric cryptographic algorithms
-------------------------------------------------

This library implements Blowfish, DES, and Triple-DES.

REQUIREMENTS

The Makefile in this distribution requires that the 'findlib' package
is already installed. 'findlib' is a small tool that simplifies platform-
independent installation.

You can get 'findlib' from:
http://www.ocaml-programming.de, or
http://www.npc.de/ocaml/linkdb

(Or you can change the Makefile, if you like.)

COMPILATION & INSTALLATION

Type
	make all
to compile a bytecode archive.

	make opt
compiles a native archive.

	make install
installs the bytecode, and if present, the native archive. The
installation directory is determined by the 'findlib' tool.

	make uninstall
removes the installation.

USAGE 

The three modules Crypt_blowfish, Crypt_des, and Crypt_3des have all the
following inner structure. The inner module Cryptsystem contains the core
cryptographic routines, you normally need only the key preparation part.
The inner module Cryptmodes implements "modes" of encryption/decryption.
The core algorithms only encrypt/decrypt 64-bit blocks, and a
"cryptographic mode" extends this to strings of arbitrary length.

let k = Crypt_blowfish.Cryptsystem.prepare "abcdefgh"

The function 'prepare' converts the key given as string to an internal
format. Key preparation may be a lengthy procedure.

let ivec = (0,0,0,0)

The initialization vector is a non-secret random number. This means that
you can transmit it with the encrypted data. A good choice is the current
time (0,0,0,0 is bad...). Some modes require not to use the same ivec
more than once for a given key in order to get maximum security.
(The ivec is a quadruple of 16 bit numbers, from MSB to LSB.)

To decrypt the message you must use the same ivec as on encryption.

let ivec', i', s' = Crypt_blowfish.Cryptmodes.encrypt_cfb64 k ivec 0 s

Here, "Cipher-feedback mode" with 64 bits is used. s' is the encrypted 
version of s. ivec' and i' are needed for cascaded invocations of cfb64.

Cascading means to encrypt a large string by calling the encryption
function with several smaller substrings. For example, if s = s1 ^ s2,
you can do:

let ivec',  i',  s1' = 
    Crypt_blowfish.Cryptmodes.encrypt_cfb64 k ivec 0 s1   in
let ivec'', i'', s2' = 
    Crypt_blowfish.Cryptmodes.encrypt_cfb64 k ivec' ai' s2

Then is s' = s1' ^ s2'. Cascading is useful with slow I/O devices.

SPEED

Blowfish is three times faster than DES which is three times faster than
Triple-DES. The algorithms have been optimized (my first DES algorithm
needed 170 seconds to encrypt 1 MB, the distributed version only 15
seconds - measured on a 486/133).

A Comparison with ssleay has shown that my library is approximately 8 to
10 times slower (ocamlopt). As Ocaml does not have 32 bit integers, all 32
bit calculations must be performed using two 16 bit numbers; this effect
explains a factor of 2. The integer representation expects a '1' in the
LSB of every integer word which makes Ocaml another bit slower. ssleay
uses static arrays which are often a bit faster; such a feature is not
available in Ocaml.

LICENSE

See the file "LICENSE".

CHANGES

0.2:	Experimental support for int32 calculations: Blowfish32

0.1.2:	Updated URLs in documentation.

