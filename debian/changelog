cryptgps (0.2.1-11) unstable; urgency=medium

  * Compile with -safe-string

 -- Stéphane Glondu <glondu@debian.org>  Mon, 12 Aug 2019 15:16:56 +0200

cryptgps (0.2.1-10) unstable; urgency=medium

  * Bump debhelper compat level to 12 and switch to dh
  * Update Vcs-*
  * Bump Standards-Version to 4.4.0
  * Switch debian/copyright to format 1.0

 -- Stéphane Glondu <glondu@debian.org>  Fri, 26 Jul 2019 13:11:29 +0200

cryptgps (0.2.1-9) unstable; urgency=low

  * Upload to unstable

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Dec 2013 08:09:43 +0100

cryptgps (0.2.1-8) experimental; urgency=low

  [ Stefano Zacchiroli ]
  * remove myself from Uploaders

  [ Stéphane Glondu ]
  * Compile with OCaml >= 4
  * Switch source package format to 3.0 (quilt)

 -- Stéphane Glondu <glondu@debian.org>  Thu, 25 Jul 2013 21:26:41 +0200

cryptgps (0.2.1-7) unstable; urgency=low

  * debian/control:
    - add myself to Uploaders
    - move to section ocaml
    - update Standards-Version to 3.8.3
    - update for dh-ocaml 0.9, and add versioned build-dependency

 -- Stéphane Glondu <glondu@debian.org>  Tue, 06 Oct 2009 01:34:54 +0200

cryptgps (0.2.1-6) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * upload to unstable
  * debian/control:
    - update build-deps: remove obsolete ones, add more strict ones to
      ensure buildability in unstable
  * debian/rules: use ocaml.mk as a CDBS "rules" snippet

  [ Mehdi Dogguy ]
  * Fix watch file.

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 26 Feb 2009 19:22:21 +0100

cryptgps (0.2.1-5) experimental; urgency=low

  [ Stefano Zacchiroli ]
  * debian/control:
    - bump Standards-Version to 3.8.0 (no changes needed)
    - add build-dep on dh-ocaml, which ships the CDBS class
  * rebuild against OCaml 3.11 (strengthen dep, for experimental build)
  * switch to debhelper 7 (bump deps and debian/compat accordingly)
  * debian/patches:
    - disable patch no_debugging_info.dpatch, we do want -g

  [ Stephane Glondu ]
  * update Homepage, fix watch file
  * Switching packaging to git

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 01 Feb 2009 19:08:38 +0100

cryptgps (0.2.1-4) unstable; urgency=low

  * promote Homepage to a real debian/control field
  * update standards-version, no changes needed
  * setting me as an uploader, d-o-m as the maintainer
  * move to debian/expand_stars.sh the ocamldoc filter to expand upstream
    comments to ocamldoc(-like) comments, to avoid make quotation issues

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 28 Dec 2007 20:16:34 +0100

cryptgps (0.2.1-3) unstable; urgency=low

  * debian/control
    - bump ocaml-nox build dep to -8, to ensure the (not so) fixed CDBS class
      is used for ocamldoc generation
  * debian/rules
    - enable generation of ocamldoc documentation (via CDBS)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 03 Sep 2007 23:03:32 +0200

cryptgps (0.2.1-2) experimental; urgency=low

  * rebuild with ocaml 3.10
  * debian/rules
    - override findlib "destdir" setting via the environment variable
      OCAMLFIND_DESTDIR instead of makefile patching, hence ...
  * debian/patches
    - ... removed now useless destdir patch

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 13 Jul 2007 09:53:46 +0200

cryptgps (0.2.1-1) unstable; urgency=low

  * Initial release (Closes: #405315).

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  2 Jan 2007 15:17:34 +0100
